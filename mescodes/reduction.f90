program reduction 

	!$ use OMP_LIB
	implicit none 
	integer:: num_du_thread
	integer::a,i,M
	
	M=10
	a=2
	
	!$OMP PARALLEL DO PRIVATE (i) SHARED(a,M)
	do i=1,M
		!$OMP ATOMIC
		a=a+1
	enddo
	!$OMP END PARALLEL DO
	
	write(*,*) "donner la valeur de a=",a
	
end program reduction

	
