program barrier

  !$ USE OMP_LIB
  IMPLICIT NONE
  integer :: num_thread
  integer :: a,b,c,d

  !$OMP PARALLEL FIRSTPRIVATE(num_thread,a)
   a=1
  num_thread=OMP_GET_THREAD_NUM();
  !$OMP SINGLE
  write(*,*) "single hello depuis le thread:",num_thread
  !$OMP END SINGLE NOWAIT

  !$OMP SINGLE
  write(*,*) " afficher la, valeur de a=", a
  !$OMP END SINGLE 

  write(*,*) "regin parallele hello depuis le thread", num_thread
  

  !$OMP MASTER
  write(*,*) "master hello depuis le thread", num_thread
  !$OMP END MASTER
  !$OMP END PARALLEL

end program barrier


