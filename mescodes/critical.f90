program criti_cal
	!$ use omp_lib
	implicit none 
	integer :: a, M
	integer :: i
	a=2
	M=10
	
	!$OMP PARALLEL PRIVATE(i) FIRSTPRIVATE(M,a)
	
	!$OMP CRITICAL 
		call cal(a,M)
	!$OMP END CRITICAL
	 !$OMP SINGLE
	 write(*,*) " la valeur finale de a=",a
	 !$OMP END SINGLE
	 
	 !$OMP END PARALLEL
	contains
	subroutine cal(a,M)
		implicit none 
		integer :: i,M,a
		
		do i=1,M
			a=a+1
		enddo
	end subroutine cal 

end program criti_cal
	
